<?php
require_once "Animal.php";
require_once "Ape.php";
require_once "Frog.php";


$sheep = new Animal("shaun");

echo "<em> Animal Name: </em>$sheep->name <br>"; // "shaun"
echo "<em> Animal Legs: </em>$sheep->legs <br>"; // 4
echo "<em> Animal Blood: </em>$sheep->cold_blooded <br><br>"; // false

$sungokong = new Ape("kera sakit");
echo "<em> Animal Name: </em>$sungokong->name <br>";
echo "<em> Animal Legs: </em>$sungokong->legs <br>"; // 4
echo "<em> Animal Blood: </em>$sungokong->cold_blooded <br>";

$sungokong->yell();

$kodok = new Frog("buduk");
echo "<em> Animal Name: </em>$kodok->name <br>";
echo "<em> Animal Legs: </em>$kodok->legs <br>";
echo "<em> Animal Blood: </em>$kodok->cold_blooded <br>"; // 2

$kodok->jump();



?>
