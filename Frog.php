<?php
require_once "Animal.php";

class Frog extends Animal {
  public $legs;


  public function __construct($name, $legs = 2, $cold_blooded = "true") {
    $this->name = $name;
    $this->legs = $legs;
    $this->cold_blooded = $cold_blooded;
  }


  public function jump() {
    echo "hop hop <br><br>";
  }

}






?>
